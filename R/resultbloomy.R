#' Creates a professionnal cart for CERMEL
#'
#' Information on cart prof is kept in Excel files
#'
#' @param id_fiche_result google sheet file containing information on diagnosis result \code{id_fiche_result}
#' @return creates a PDF report
#'
#' @keywords keywords123
#'
#' @export
#'
#' @import dplyr
#' @import knitr
#' @import lubridate
#' @import readxl
#' @import rmarkdown
#' @import stringr
#' @import tidyr
#' @import xtable
#' @import fs
#' @import here
#' @import knitr
#' @import tinytex
#' @import googlesheets4
#' @importFrom stringi stri_trans_general
#' 

BloomyGen <- 
  function(id_fiche_result, plaque){

  logo_cermel <- fs::path(fs::path_package(getPackageName()), "logo", "Logo_cermel.png")
  
  signature_RB <- fs::path(fs::path_package(getPackageName()), "signatures", "sig_rodrigue_bikangui_stamp.jpg")
  
  database <-
    googlesheets4::read_sheet(id_fiche_result, sheet = "data") %>% 
    dplyr::rename_all(~(c("Id_patient", "follow_up", "date_collecte", "date_diagn", "ct_value",
                          "comment", "conclusion", "kit", "methode", "equipment", "plaque_num"))) %>%
    dplyr::filter(plaque_num == plaque) %>%
    dplyr::mutate_at(vars(starts_with("ct_")), ~(as.numeric(gsub("Ind", NA, .)))) %>%
    dplyr::mutate(Id_patient = paste(Id_patient, follow_up),
                  result = case_when(str_detect(str_to_lower(conclusion), "positi") ~ "Positif/Positive",
                                     TRUE ~ "Négatif/Negative"),
                  conclusion = case_when(result == "Négatif/Negative" ~ "L'ADN plasmodium non détecté/DNA of plasmodium not detected",
                                         result == "Positif/Positive" ~ "L'ADN plasmodium détecté/DNA of plasmodium detected",
                                         TRUE ~ "Invalide / erreur")) %>%
    dplyr::arrange(conclusion)
  
  date_diag <- max(database$date_diagn) 
  
  year <- stringr::str_extract(date_diag, "20[0-9]{2}")
  yearmonth <- stringr::str_extract(date_diag, "20[0-9]{2}-[0-9]{2}")
  date <- stringr::str_extract(date_diag, "20[0-9]{2}-[0-9]{2}-[0-9]{2}")
  
  dir_cartyear <- here::here(year)
  
  
  
  
  dir_yrmnth <- here::here(dir_cartyear, yearmonth)
  dir_report <- fs::path(dir_yrmnth)
  fs::dir_create(dir_report)
  
  plaque_id <- unique(database$plaque_num)
  
  rmd_outpath <- fs::path(fs::path_package(getPackageName()), "Rmd", "forms_parent.Rmd")
  outpath_DIR <- paste0(dir_report, "/Resultat_", plaque_id, "_BLOOMY_PCR_", format(Sys.Date(), "%Y-%m-%d"))
  rmarkdown::render(rmd_outpath, output_file = outpath_DIR, quiet = TRUE)
  cat("\nLe Lesultat du ", paste0(plaque_id,"_BLOOMY_PCR_", date, ".pdf est généré dans le dossier ", yearmonth))

}

